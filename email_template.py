# coding: utf-8
""" Email Tamplate with UTF-8 Support.
Supported variables $name, $to_addr, $last_payment, $days_due
"""

SUBJECT = 'Δωρεά HSGR - $name'
BODY = """\
$name <$to_addr>

$phrase

Τρόποι πληρωμής:
* Μετρητά: Αλέξανδρος Τσουράπας, Άγης
* PayPal: donate@hackerspace.gr
* Τράπεζα Πειραιώς IBAN: GR3201720820005082079678770

Αν υπάρχει σχετικό πρόβλημα παρακαλώ στείλτε email στο: ebill@hackerspace.gr.

Εδώ, https://zisi.gitlab.io/budget/ μπορείτε να δείτε τα έσοδα/έξοδα του προηγούμενου μήνα.

Αυτοματοποιημένο email.

Ευχαριστούμε!
"""
